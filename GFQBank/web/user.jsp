<%-- 
    Document   : user
    Created on : Mar 9, 2020, 7:29:27 PM
    Author     : this PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome, ${sessionScope.loginUser.name}</h1>
        <table>
            <th>Work</th>
            <td>${employee.workschedule}</td>
        </table>
        <table>
            <th>Leave record</th>
            <td>${employee.leaverecord}</td>
        </table>
    </body>
</html>
